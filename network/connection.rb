require_relative '../data/networkData'
require "net/http"
require 'nokogiri'
require 'singleton'

class Connection
  include Singleton

  def connect (urlStr)
    puts "connecting #{urlStr} ..."
    begin
      uri = URI(urlStr)
      res =  Net::HTTP.get_response(uri)
      if res.is_a?(Net::HTTPSuccess)
        doc = Nokogiri::HTML(res.body)
      else
        puts "failed an response is #{res}"
        puts res.body
      end
    rescue Exception
      raise
    end
  end

  def tryToFetchWeibo
    # this is a greate job to do ....
  end

end



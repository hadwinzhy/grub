require_relative '../data/networkData'
require_relative '../network/connection'
require_relative '../data/companyData'
require_relative '../util/myFile'


class GrubController
  attr_reader :urlList, :companyAllList

  def initialize
    @urlList = []
    @dataList = []
    @companyAllList = []
  end

  def tryToAsk
    #try to loadfile ../data/urlList.txt
    File.foreach("data/urlList.txt") do |line|
      #try to ask whether the url u want to grub and add to @urlList
      website = line.strip
      if website.size == 0 || website[0] == '#'
        next
      end
      print "Do u want to grub: #{website} (y/n)? : "
      answer = gets.chomp
      if answer[0].eql?("y")
        @urlList.push (website)
      end
    end
  end

  def grub
    #try to grub the data from each data in @dataList
    #create data which will be send to network
    @urlList.each do |name|
      networkData = NetworkData.create(name)
      while (true)
        htmlData = Connection.instance.connect(networkData.url)
        if htmlData == nil
          #networkData.nextPage()
          #next
          break
        else
          #add to list
          companyInOnePage = CompanyDataFactory.getCompanyInOnePage(htmlData, networkData.key)
          if companyInOnePage != nil
            MyFile.writeArrayToFileWithJson(companyInOnePage, "data.txt")
            @companyAllList += companyInOnePage
          end
          networkData.nextPage()
        end
      end
    end
    
  end

  #main method
  if __FILE__ == $0
    controller = GrubController.new
    controller.tryToAsk
    controller.grub
  end

end

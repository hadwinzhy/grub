require 'nokogiri'
require_relative '../network/connection'
require_relative '../util/parseHTML'

module CompanyDataFactory
  @companyListInOnePage
  
  def self.getCompanyInOnePage ( htmlRawData, key )
    #create data by networkData
    case key
    when $kr36
      getDetailUrlByCss(htmlRawData, 'div.caption h5.block a')
    else
      puts "no such key #{key}"
      return
    end

    @companyListInOnePage.each do |company| 
      htmlRawData = Connection.instance.connect(company.detailURL)
      if htmlRawData == nil
        next
      else
        company.getDetailInfo(htmlRawData, key)
      end
    end
    return @companyListInOnePage
  end

  def self.getDetailUrlByCss(htmlRawData, cssStyle)
    @companyListInOnePage = []
    htmlRawData.css(cssStyle).each do |link|
      content = link.content.strip
      if content.size != 0
        company = CompanyData.new
        company.name = content
        company.detailURL = link.values[0]
        @companyListInOnePage.push(company)
      end
    end
  end

end


class CompanyData
  attr_accessor :name, :type,:detailURL, :companyWebsite, :ceoName, :weiboURL, :isNeedMan, :isProductOnline,:description
  
  def getDetailInfo(htmlRawData, key)
    case key
    when $kr36
      detailData = htmlRawData.css('div.caption.span9')
      self.companyWebsite = detailData.at_css('small a')['href']
#      self.ceoName = detailData.at_css('label')
      self.weiboURL = ParseHTML.parseLink(detailData, 'a.hori.inline-block')
#      self.ceoName = ParseHTML.parseLinkValue(detailData, 'i.icon-founder label')
      self.ceoName = ParseHTML.parseLinkValue(detailData, 'label.inline-block')
      self.type = []
      detailData.css('div.wrapper-header+p+p').each do |content|
        tag = content.css('a')
        if tag != nil
          self.type.push(tag.inner_text())
        end
      end
    end
  end

  def to_s
    puts "name is #{self.name}"
    puts "detailURL is #{self.detailURL}" 
    puts "website is #{self.companyWebsite}"
    puts "ceoName is #{self.ceoName}"
    puts "weiboURL is #{self.weiboURL}"
    puts "type is #{self.type}\n"
  end

  def to_json(*a)
    {
      "name" => self.name,
      "detailURL" => self.detailURL,
      "website" => self.companyWebsite,
      "ceoName" => self.ceoName,
      "weiboURL" => self.weiboURL,
      "type" => self.type
    }.to_json(*a)
  end
end

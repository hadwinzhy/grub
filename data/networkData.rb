$kr36 =  "www.36kr.net"
$startup17 = "www.17startup.com"

class NetworkData
  @@subclasses = {}
  attr_reader :url, :key, :baseURL
  attr_writer :curPage
  def initialize
  end

  def self.create type
    myClass = @@subclasses[type]
    if myClass
      myClass.new 
    else 
      raise "No such type : #{type}"
    end
  end

  def self.register_reader name
    @@subclasses[name] = self
  end

  def nextPage
    @curPage += 1 
  end
end


class NetworkDataOf36Kr < NetworkData
  register_reader $kr36
  def initialize
    super
    @curPage = -1
    @baseURL = "http://www.36kr.net/product/list/"
    self.nextPage
    @key = $kr36
  end
  
  def nextPage
    super
    @url = @baseURL +  @curPage.to_s + "?location=11"
  end
end


class NetworkDataOf17Startup < NetworkData
  register_reader $startup17
  def initialize
    super
    @curPage = 0
    @baseURL = "http://17startup.com/startup?page="
    self.nextPage
    @key = $startup17
  end
  
  def nextPage
    super
    @url = "#{@baseURL}#{@@curPage.to_s}"
  end

end

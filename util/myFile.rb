require 'json'

module MyFile
  def self.writeArrayToFileWithJson(arrayData, filePath)
    json = ''
    arrayData.each do |data|
      json += data.to_json
    end
    file = File.open(filePath, 'w')
    file.write(json)
    file.close()
  end

end

require 'nokogiri'

module ParseHTML  
  def self.parse(html, cssStyle)
    value = html.at_css(cssStyle)
  end

  def self.parseLink(html, cssStyle)
    value = html.at_css(cssStyle)
    if value != nil
      return value['href']
    else
      return nil
    end
  end

  def self.parseLinkValue(html, cssStyle)
    value = self.parse(html,cssStyle)
    if value != nil
      return value.inner_text.strip
    else
      return nil
    end
  end

end
